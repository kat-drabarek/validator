$('form').on('submit', function(e){
	$('.valid-error').remove();
	checkValidationRule(e, this, require);
	checkValidationRule(e, this, text);
	checkValidationRule(e, this, pesel);
	checkValidationRule(e, this, today);
	checkValidationRule(e, this, email);
	checkValidationRule(e, this, date);
	$('.valid-error').css("color", "red");
});

function checkValidationRule(e, form, rule){
	var errorSource = [];
	$('.' + rule.className, form).each(function(){
		if(!rule.check($(this).val())){
			errorSource.push($(this));
		}
	});
	if(errorSource.length > 0){
		$.each(errorSource, function(){
			$(this).after("<span class='valid-error'>  " + rule.errorMessage + "</span>");
		});
		e.preventDefault();
	}
}

// ValidationRule prototype:
// check is a function(inputValue)
function ValidationRule(className, errorMessage, check){
	this.className = className;
	this.errorMessage = errorMessage;
	this.check = check;
}

// ValidationRule instances:
var require = new ValidationRule("valid-required", "Required field.", function(inputValue){
	if(!inputValue){
		return false;
	}
	return true;
});

var text = new ValidationRule("valid-text", "Invalid text.", function(inputValue){
	if(!inputValue.match(/^[a-z]+$/ig) && inputValue){
		return false;
	}
	return true;
});

var pesel = new ValidationRule("valid-pesel", "Invalid PESEL.", function(inputValue){
	if(!inputValue.match(/^\d{11}$/g) && inputValue){
		return false;
	}
	return true;
});

var today = new ValidationRule("valid-today", "Invalid day.", function(inputValue){
	var currentDay = ["niedziela","poniedzialek","wtorek","sroda","czwartek","piatek","sobota"][(new Date()).getDay()];
	if(inputValue !== currentDay && inputValue){
		return false;
	}
	return true;
});

var email = new ValidationRule("valid-email", "Invalid email address.", function(inputValue){
	if(!inputValue.match(/^\S*@\S*\.\S+$/ig) && inputValue){
		return false;
	}
	return true;
});

var date = new ValidationRule("valid-date", "Invalid date.", function(inputValue){
	if(inputValue.match(/^\d{4}\-(0[1-9]|1[0-2])\-(0[1-9]|1[0-9]|2[0-9]|3[0-1])$/g)){
		if(!checkDateInCalendar(inputValue)){
			return false;
		}
	}
	else if (inputValue){
		return false;
	}
	return true;
});

// Additional function checking if given date is a valid calendar combination, ie. 30th February would be invalid.
function checkDateInCalendar(text){
	var comp = text.split('-');
	var y = parseInt(comp[0], 10);
	var m = parseInt(comp[1], 10);
	var d = parseInt(comp[2], 10);
	var date = new Date(y,m-1,d);
	if (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d){
		return true;
	}
	else {
		return false;
	}
}
