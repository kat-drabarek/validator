# README #

JS Validator by Kat Drabarek.

### How to start: ###

* add jQuery to your HTML, e.g. <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
* add Validator scripts: <script src="validator.js"></script> at the bottom of your body 

### Validator has six basic validation rules: ###

* **require** - checks if all required inputs have been filled.  
Add class **'valid-required'** to your inputs.

* **text** - checks if input value contains only one word.  
Add class **'valid-text'** to your inputs.

* **pesel** - checks if input value contains only 11 digits.  
Add class **'valid-pesel'** to your inputs.

* **today** - checks if selected day matches current day of the week.  
Add class **'valid-today'** to your selects.

* **email** - checks if input value contains email address (format: characters[at]characters[dot]characters).  
Add class **'valid-email'** to your inputs.

* **date** - checks if input contains date (format: yyyy-mm-dd) and if given date is a valid calendar combination.  
Add class **'valid-date'** to your inputs.

### Adding custom validation rules: ###
To add a custom validation rule go to validator.js and create a new instance of **ValidationRule(className, errorMessage, check)**, where 'check' is a function(inputValue) containing a validation mechanism, e.g. matching regex.  
On form submit call the function checkValidationRule(e, this, instanceName), where instanceName is the name of your custom ValidationRule.